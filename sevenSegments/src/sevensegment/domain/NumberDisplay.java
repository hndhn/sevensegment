package sevensegment.domain;

public interface NumberDisplay {

	public void display(int number);
	public boolean[] finalSegments();
}
