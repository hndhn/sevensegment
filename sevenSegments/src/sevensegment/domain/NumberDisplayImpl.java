package sevensegment.domain;

public class NumberDisplayImpl implements NumberDisplay {
	private SevenSegment segments;

	public void display(int number) {
		segments = new setLedByLastDigit();
		numberInterpret(number, segments);
	}

	public boolean[] finalSegments() {
		return segments.getSegments();
	}
	private void numberInterpret(int num, SevenSegment segments) {
		if (num == 0) {
			segments.setLED(0, true);
			segments.setLED(1, true);
			segments.setLED(2, false);
			segments.setLED(3, true);
			segments.setLED(4, true);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
		if (num == 1) {
			segments.setLED(0, false);
			segments.setLED(1, false);
			segments.setLED(2, false);
			segments.setLED(3, true);
			segments.setLED(4, false);
			segments.setLED(5, false);
			segments.setLED(6, true);
		}
		if (num == 2) {
			segments.setLED(0, true);
			segments.setLED(1, false);
			segments.setLED(2, true);
			segments.setLED(3, true);
			segments.setLED(4, true);
			segments.setLED(5, false);
			segments.setLED(6, false);
		}
		if (num == 3) {
			segments.setLED(0, true);
			segments.setLED(1, false);
			segments.setLED(2, true);
			segments.setLED(3, true);
			segments.setLED(4, false);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
		if (num == 4) {
			segments.setLED(0, false);
			segments.setLED(1, true);
			segments.setLED(2, true);
			segments.setLED(3, true);
			segments.setLED(4, false);
			segments.setLED(5, false);
			segments.setLED(6, true);
		}
		if (num == 5) {
			segments.setLED(0, true);
			segments.setLED(1, true);
			segments.setLED(2, false);
			segments.setLED(3, false);
			segments.setLED(4, false);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
		if (num == 6) {
			segments.setLED(0, true);
			segments.setLED(1, true);
			segments.setLED(2, true);
			segments.setLED(3, false);
			segments.setLED(4, true);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
		if (num == 7) {
			segments.setLED(0, true);
			segments.setLED(1, false);
			segments.setLED(2, false);
			segments.setLED(3, true);
			segments.setLED(4, false);
			segments.setLED(5, false);
			segments.setLED(6, true);
		}
		if (num == 8) {
			segments.setLED(0, true);
			segments.setLED(1, true);
			segments.setLED(2, true);
			segments.setLED(3, true);
			segments.setLED(4, true);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
		if (num == 9) {
			segments.setLED(0, true);
			segments.setLED(1, true);
			segments.setLED(2, true);
			segments.setLED(3, true);
			segments.setLED(4, false);
			segments.setLED(5, true);
			segments.setLED(6, true);
		}
	}

}
