package sevensegment.domain;

public interface SevenSegment {
	public void setLED(int LED, boolean on);
	public boolean[] getSegments();
}
