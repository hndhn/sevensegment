package sevensegment.domain;

public class setLedByLastDigit implements SevenSegment {
	private static int numberOfSegments = 7;
	private boolean[] segments = new boolean[numberOfSegments];
	public setLedByLastDigit() {
	}
	@Override
	public void setLED(int LED, boolean on) {
		segments[LED] = on;
	}
	
	public boolean[] getSegments() {
		return segments;
	}
	
}
