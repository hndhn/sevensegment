package sevensegment.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestSevenSegment {
	NumberDisplay subject;
	 @Before
	  public void setUp() {
		 subject = new NumberDisplayImpl();
	  }

	@Test
	public void rendersLastDigitIfMoreThanOne() {
		subject.display(3);
		String temp ="";
		for (int i=0; i<subject.finalSegments().length; i++) temp = temp + subject.finalSegments()[i] +" ";
		assertEquals("Display is shows a 3", "true false true true false true true ", temp);
	}
	@Test
	public void rendersOne() {
		subject.display(1);
		String temp ="";
		for (int i=0; i<subject.finalSegments().length; i++) temp = temp + subject.finalSegments()[i] +" ";
		assertEquals("Display is shows a 1", "false false false true false false true ", temp);
	}
}
